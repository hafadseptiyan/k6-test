import http from 'k6/http';
import { sleep, check } from 'k6';

export let options = {
  stages: [
    { duration: '30s', target: 10 },  
    { duration: '1ms', target: 25 },  
    { duration: '1m10s', target: 150 }, 
    { duration: '20s', target: 50 }, 
  ],
};

export default function () {
  const response = http.get('https://run.mocky.io/v3/1b6d9480-44c4-49ae-a095-27e780edf0eb');
  check(response, { 'status is 200': (r) => r.status === 200 });
  sleep(1);
}
